class Review < ActiveRecord::Base
  default_scope -> { order('created_at DESC') }
  validates :text, presence: true
end
