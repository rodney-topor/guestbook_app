# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
GuestbookApp::Application.config.secret_key_base = 'df41798e06441e5ebfd625b5bf90d9ecfdcbe8bbfe74ca94571ad4664a0fd45925260393fd3533305bc4802dfe75eb2552fad6824476285700b2f0df79e838a5'
